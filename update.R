# Plot most up-to-date CORONA data from Johns Hopkins. 
# Plot real increase against 33% daily increase since a certain threshold.
# Cf. paywalled graph on Financial Times
# https://www.ft.com/content/a26fbf7e-48f8-11ea-aeb3-955839e06441

# setwd("/Users/vfl262/Desktop/COVID/")

library(tidyr)
library(ggplot2)
library(plyr)

# Get Data from Johns Hopkins
# https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series
url <- "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv"
cnf_jh <- read.csv(url, stringsAsFactors = FALSE)

url <- "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Deaths.csv"
dth_jh <- read.csv(url, stringsAsFactors = FALSE)

url <- "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Recovered.csv"
rcv_jh <- read.csv(url, stringsAsFactors = FALSE)

# Define countries to plot
countries <- c("Austria", "Germany", "Belgium", "Italy", "Spain", "US", 
               "Denmark", "France", "United Kingdom",
               "Netherlands", "Luxembourg", "Iran")

# define threshold at which to start the plot (cnf = confirmed; dth = deaths, rcv = recovered)
thr_cnf <- 100
thr_dth <- 1
thr_rcv <- 1

# Define function to reshape JH data for plotting
reshape_df <- function(df, countries, threshold){
  # Takes JH data, selects countries of interest, aggregates on a country-level,
  # reshapes wide to long format, and converts dates for ggplot.
  
  df <- df[df$Country.Region %in% countries, -c(1, 3, 4)]
  df <- aggregate(df[, -1], by=list(df$Country.Region), FUN=sum)
  df <- gather(df, date, count, names(df)[-1])
  names(df)[1] <- "country"
  df <- df[df$count > threshold, ]
  df <- df[order(df$country),]
  row.names(df) <- 1:nrow(df)
  
  # There's probably a smarter way to do this, but it works...
  df$days_since <- NA
  counter <- 1
  
  # Add label bool for last observation for country
  df$label <- FALSE
  
  prev_country <- df[1, "country"]
  df[1, "days_since"] <- 1
  
  for(row in 2:nrow(df)){
    now_country <- df[row, "country"]

    if(prev_country != now_country){
      counter <- 1
      df[row-1, "label"] <- TRUE
    }else{
      counter <- counter + 1
    }
    
    df[row, "days_since"] <- counter
    prev_country <- now_country
  }
  
  df[nrow(df), "label"] <- TRUE
  
  return(df)
}

# Prepare data for plotting with the above function reshape_df
cnf <- reshape_df(cnf_jh, countries, threshold = thr_cnf)
cnf$kind <- "Confirmed"

dth <- reshape_df(dth_jh, countries, threshold = thr_dth)
dth <- merge(cnf[, c("country", "date", "days_since")], dth[, c("country", "date", "count", "label")], by=c("country", "date"))
dth$kind <- "Deaths"

rcv <- reshape_df(rcv_jh, countries, threshold = thr_rcv)
rcv <- merge(cnf[, c("country", "date", "days_since")], rcv[, c("country", "date", "count", "label")], by=c("country", "date"))
rcv$kind <- "Recovered"

plot_df <- rbind(cnf, dth)
plot_df <- rbind(plot_df, rcv)

# Adapt y-scale in plots automatically
set_breaks = function(limits) {
  round_any(exp(seq(log(limits[1]), log(limits[2]), length.out=20)), 
           accuracy=1)
}

write.csv(dth, "data/deaths_days.csv")
# Calculate 33 percentage increase since beginning for current max days since
# first xx cases
log_cnf_33 <- thr_cnf
log_dth_33 <- thr_dth
log_rcv_33 <- thr_rcv

for(x in 1:max(plot_df$days_since)){
  log_cnf_33 <- log_cnf_33 * 1.33
  log_dth_33 <- log_dth_33 * 1.33
  log_rcv_33 <- log_rcv_33 * 1.33
}

line <- data.frame(days_since=c(1, max(cnf$days_since), 
                                1, max(dth$days_since),
                                1, max(rcv$days_since)),
                   count=c(thr_cnf, log_cnf_33, 
                           thr_dth, log_dth_33,
                           thr_rcv, log_rcv_33),
                   kind=c("Confirmed", "Confirmed", 
                          "Deaths", "Deaths",
                          "Recovered", "Recovered"))


gg <- ggplot(plot_df, aes(x=days_since, y=count)) +
  theme_bw() +
  theme(legend.position = "none") +
  geom_line(data=line, linetype="longdash") +
  geom_line(aes(color=country)) +
  geom_point(aes(color=country)) + 
  geom_label(data=plot_df[plot_df$label==TRUE,], 
             aes(label=country, fill=country), 
             colour = "white", hjust = 0, nudge_x = 0.2,
             alpha=.8) + 
  scale_y_continuous(trans = 'log', breaks=set_breaks) +
  scale_x_continuous(breaks=seq(1, max(plot_df$days_since, 1))) +
  facet_wrap(~kind, ncol=1, scales="free_y") +
  labs(x = paste0("Days since the first ", thr_cnf," cases"), 
       y = "Logatithmic Scale (33% daily increase)")
 gg

pdf(paste0("output/", Sys.time(), ".pdf"), width=12, height=12)
gg
dev.off()
