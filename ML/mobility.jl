using CSV, DataFrames
using HTTP
using Dates

countries = Dict{String, String}(
               "AUS" => "Australia",
               "AUT" => "Austria", 
               "BEL" => "Belgium",
               "BRA" => "Brazil", 
               "CHN" => "China",
               "CZE" => "Czech Republic",
               "DEN" => "Denmark",
               "DEU" => "Germany", 
               "ESP" => "Spain",
               "FRA" => "France",
               "GBR" => "United Kingdom",
               "ITA" => "Italy",
               "JPN" => "Japan",
               "KOR" => "Republic of Korea",
               "NLD" => "Netherlands",
               "NZL" => "New Zealand",
               "SWE" => "Sweden",
               "CHE" => "Switzerland",
               "USA" => "United States"
             )



data_frames = Dict()
# Create individual DataFrames
for country ∈ keys(countries), file in readdir("data/")
  dfname = "dat_$country" |> Symbol
  df = CSV.read(joinpath("data", "$country.csv"))
  @eval $dfname = $df
  push!(data_frames, dfname => df)
end





#=
 =Google Mobility Data
 =#
google = "https://www.gstatic.com/covid19/mobility/Global_Mobility_Report.csv"

function getgoogle(url)
  h = HTTP.get(url)
  go = h.body |> String
  write("googleraw.txt", go)
end

getgoogle(google)

# Format CSV
ggl = CSV.read("googleraw.txt") |> DataFrame
ggl = ggl[:, Not([:country_region_code, :sub_region_1, :sub_region_2, :metro_area, :iso_3166_2_code, :census_fips_code])]
rename!(ggl, "date" => "Date")
ggl = filter(x -> x.country_region ∈ values(countries), ggl)

# adding Google Mobility Data to Policy Data Frames
for (CTR, country) in countries
  mob = filter(x -> x.country_region == country, ggl) 
  # join with policy measures DF
  ex = "dat_$CTR" |> Meta.parse 
  tmp = join(eval(ex), mob, on = :Date, kind = :left)
  @eval Main $ex = $tmp
  data_frames[ex] = eval(tmp)
end

#=
 =Apple Mobility Data
 =#
"""
    getapple(t)

Call the apple Covid API to get the latest CSV on mobility data.
Since the latest available date is unknown and subject to change, recursively
query the latest date, starting on current day.
"""
function getapple(t)
  ap1 = "https://covid19-static.cdn-apple.com/covid19-mobility-data/"
  ap2 = "2015HotfixDev15/v3/en-us/"
  apdate = Dates.format(Dates.today()-Dates.Day(t), "yyyy-mm-dd")
  ap3 = "applemobilitytrends-$apdate.csv"
  println(ap1 * ap2 * ap3)
  try
    HTTP.get(ap1 * ap2 * ap3)
  catch e
    if isa(e, HTTP.ExceptionRequest.StatusError)
      println("No success, trying with earlier date")
      getapple(t + 1)
    end
  end
end

apl = getapple(0)
apl = apl.body |> String;
write("appleraw.txt", apl)
csv = CSV.read("appleraw.txt")
CSV.write("applemobility.csv", csv)

#filter out unneeded columns
apl = filter(x -> x.region ∈ values(countries), csv)
rename!(apl, Dict(Symbol("sub-region") => "sub_region"))
apl = apl[:, Not([:geo_type, :alternative_name, :sub_region, :country]) ]


apl = DataFrames.stack(apl)
apl.variable = apl.variable .|> String #coerce to string format


# adding Apple Mobility Data to Policy Data Frames
for (CTR, country) in countries
  mob = filter(x -> x.region == country, apl) 
  rename!(mob, :variable => :Date)
  mob = unstack(mob, :Date, :transportation_type, :value)
  # @show head(mob)
  rename!(mob, Dict("$name" => "apple_$name" for name in names(mob)[2:end]))
  mob.Date = Dates.Date.(mob.Date)
  # join with policy measures DF
  ex = "dat_$CTR" |> Meta.parse 
  tmp = join(eval(ex), mob, on = :Date, kind = :left)
  @eval Main $ex = $tmp
  data_frames[ex] = eval(tmp)
end



data_frames


# Save the country-specific data frames to files
for countryname in keys(countries)
  eval(data_frames[Symbol("dat_$countryname")]) |> CSV.write("data/$countryname.csv")
end
