using CSV, DataFrames, DataFramesMeta
using HTTP, JSON
using Dates, Tables
using XLSX

startpoint = "2020-01-01" |> Date
endpoint = Dates.today() #- Day(3)

# Splitting data for training
# training_tl = startpoint:Day(1):endpoint
# test_tl = endpoint:Day(1):today()
timeline = startpoint:Day(1):endpoint


# download recent data to directory
address = "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/$startpoint/$endpoint"


h = HTTP.get(address)
t = h.body |> String;
dat = JSON.parse(t)



#=
 =Country selection
 =#

countries = Dict{String, String}(
               "AUS" => "Australia",
               "AUT" => "Austria", 
               "BEL" => "Belgium",
               "BRA" => "Brazil", 
               "CHN" => "China",
               "CZE" => "Czech Republic",
               "DEN" => "Denmark",
               "DEU" => "Germany", 
               "ESP" => "Spain",
               "FRA" => "France",
               "GBR" => "United Kingdom",
               "ITA" => "Italy",
               "JPN" => "Japan",
               "KOR" => "Republic of Korea",
               "NLD" => "Netherlands",
               "NZL" => "New Zealand",
               "SWE" => "Sweden",
               "CHE" => "Switzerland",
               "USA" => "United States"
             )



datestrings = Dates.format.(timeline, "yyyy-mm-dd")
convert(d::Date) = Dates.format(d, "dduyyyy")  |> Base.Unicode.lowercase
convert(d::Date, s::String) = Dates.format(d, s) |> Base.Unicode.titlecase


"""
Get data for deaths from the Johns Hopkins dataset.
"""
function get_deaths(country, datestrings)
  v = []
  for d ∈ datestrings
    try
     a = dat["data"][d][country]["deaths"]
     push!(v, a)
    catch e
      push!(v, e)
    end
    end
    return v
end


data_frames = Dict()
# Create individual DataFrames
for country ∈ keys(countries)
  dfname = "dat_$country" |> Symbol
  @eval $dfname = DataFrame(
                            :Date => timeline, 
                            :Deaths => get_deaths($country, datestrings)
                           )
  push!(data_frames, country => dfname)
end

#=
 =get Data for Policy Measures
 =#
pol = "https://github.com/OxCGRT/covid-policy-tracker/raw/master/data/timeseries/OxCGRT_timeseries_all.xlsx"
download(pol, "policytable.xlsx")
poltable = XLSX.openxlsx("policytable.xlsx")

"""
Macro that allows to interactively choose variables from a list.
Returns a subset of the input list as a vector
"""
macro choosevars(list)
  vars = []
  for name in eval(list)
    println("keep $name? [Y/n]")
    action = readline(stdin)
    action != "n" ? push!(vars, name) : continue
  end
  return vars
end


#=
 =Applies the selection, needs input in the REPL/terminal.
 =Comment out if you run the script repeatedly, and don't want to enter selection again!
 =#
# selection = @choosevars XLSX.sheetnames(poltable) #needs manual selection in REPl


"""
  this is a workaround because for some reason Julia cannot convert the 
  string format 'dduY' to a date, throwing an error.
   The function returns a string with spaces added, which then can be converted to DateFormat.
"""
function datestring_add_space(τ::String)
  d = Base.SubString(τ, 1, 2)
  y = Base.SubString(τ, 6)
  m = Base.SubString(τ, 3, 5)
  return "$d $m $y"
end


#=
 = Combine data for chosen policy measures into the country-specific data frames
 =#
#
for sheetname in selection
  sheeit = XLSX.openxlsx("policytable.xlsx") do xf
    DataFrame(XLSX.gettable(xf[sheetname])...)
  end
  # datecols = [ convert(d) for d ∈ timeline if occursin.(convert(d), names(sheeit)) ] .|> Symbol
  datecols = [ name for name in names(sheeit) if name ∈ String.(convert.(timeline)) ] .|> Symbol
  df = filter(x -> x.CountryCode ∈ keys(countries), sheeit)
  df = DataFrames.stack(df, datecols, variable_name =:Date, value_name=Symbol("$sheetname"), view=true)
  df.Date = df.Date .|> String .|> Base.Unicode.titlecase .|> datestring_add_space
  ddf = DateFormat("dd u Y")
  df.Date = Date.(df.Date, ddf)
  for country in keys(countries)
    ex = "dat_$country" |> Meta.parse #still don't fully understand metaprogramming
    sel = filter(x -> x.CountryCode == country, df) #select countryname
    sel = sel[:, Not([:CountryName, :CountryCode])] 
    tmp = join(eval(ex), sel, on = :Date, kind = :left)
    @eval Main $ex = $tmp
  end
end


#change KeyErrors to missing values
for dat in values(data_frames)
  df = eval(dat).Deaths
  df[isa.(df, KeyError)] .= missing
  df[isa.(df, Nothing)] .= missing
end


# Save the country-specific data frames to files
for countryname in keys(data_frames)
  eval(data_frames[countryname]) |> CSV.write("data/$countryname.csv")
end
